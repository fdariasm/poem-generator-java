package fdariasm.ast;

import java.util.Map;

public class ComposedWord extends PoemTree{

	private PoemTree words;
	
	private PoemTree or;

	public ComposedWord(Map<String, PoemTree> rules, PoemTree words, PoemTree or) {
		super(rules);
		this.words = words;
		this.or = or;
	}

	@Override
	public String getPoem() {
		return words.getPoem() + " " + or.getPoem();
	}
}
