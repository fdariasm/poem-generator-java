package fdariasm.ast;

import java.util.List;
import java.util.Map;

public class ConcatDef extends PoemTree{

	private List<PoemTree> childs;

	public ConcatDef(Map<String, PoemTree> rules, List<PoemTree> childs) {
		super(rules);
		this.childs = childs;
	}

	@Override
	public String getPoem() {
		String res = "";
		for(PoemTree pt : childs){
			res += pt.getPoem() + " ";
		}
			
		return res;
	}
	
	
}
