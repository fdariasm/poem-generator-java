package fdariasm.ast;

import java.util.List;
import java.util.Map;

public class Or extends PoemTree{

	private List<PoemTree> childs;

	public Or(Map<String, PoemTree> rules, List<PoemTree> childs) {
		super(rules);
		this.childs = childs;
	}
	
	@Override
	public String getPoem() {
		PoemTree poemTree = childs.get(getRandom(childs.size()));
		return poemTree.getPoem();
	}
}
