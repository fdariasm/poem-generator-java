package fdariasm.ast;

import java.util.List;
import java.util.Map;

public class Words extends PoemTree{

	private List<String> words;

	public Words(Map<String, PoemTree> rules, List<String> words) {
		super(rules);
		this.words = words;
	}
	
	@Override
	public String getPoem() {
		return words.get(getRandom(words.size()));
	}
}
