package fdariasm.ast;

import java.util.Map;

public class Reference extends OrDef{

	private String reference;

	public Reference(Map<String, PoemTree> rules, String reference) {
		super(rules);
		this.reference = reference;
	}
	
	@Override
	public String getPoem() {
		PoemTree poemTree = rules.get(reference);
		
		return poemTree.getPoem();
	}
}
