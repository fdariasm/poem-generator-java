package fdariasm.ast;

import java.util.Map;

public class Keyword extends OrDef{

	private String value;

	public Keyword(Map<String, PoemTree> rules, String value) {
		super(rules);
		this.value = value;
	}	
	
	@Override
	public String getPoem() {
		if("$END".equals(value)) return "";
		return "\n";
	}
}
