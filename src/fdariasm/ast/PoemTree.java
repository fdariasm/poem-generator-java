package fdariasm.ast;

import java.util.Date;
import java.util.Map;
import java.util.Random;

public abstract class PoemTree {

	protected Map<String, PoemTree> rules;
	
	protected Random random = new Random(new Date().getTime());

	public PoemTree(Map<String, PoemTree> rules) {
		this.rules = rules;
	}
	
	public String getRuleName(){ return ""; }
	
	public PoemTree getDefinition(){ return null; }
	

	public abstract String getPoem();
	
	protected int getRandom(int n){
		
		return random.nextInt(n);
	}
}
