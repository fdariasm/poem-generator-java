package fdariasm.ast;

import java.util.Map;

public class Definition extends PoemTree{

	private String rule;
	
	private PoemTree definition;
	

	public Definition(Map<String, PoemTree> rules, String rule,
			PoemTree definition) {
		super(rules);
		this.rule = rule;
		this.definition = definition;
	}
	
	@Override
	public PoemTree getDefinition() {
		return definition;
	}
	
	@Override
	public String getRuleName() {
		return rule;
	}
	
	@Override
	public String getPoem() {
		return definition.getPoem();
	}
}
