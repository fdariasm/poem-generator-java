package fdariasm.composer;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;

import fdariasm.ast.PoemTree;
import fdariasm.parser.PoemGeneratorBaseListener;
import fdariasm.parser.PoemGeneratorLexer;
import fdariasm.parser.PoemGeneratorParser;
import fdariasm.parser.PoemGeneratorParser.DefinitionContext;

public class PoemComposer {

	protected static String[] lines = new String[] {
			"POEM: <LINE> <LINE> <LINE> <LINE> <LINE>",
			"LINE: <NOUN>|<PREPOSITION>|<PRONOUN> $LINEBREAK",
			"ADJECTIVE: black|white|dark|light|bright|murky|muddy|clear <NOUN>|<ADJECTIVE>|$END",
			"NOUN: heart|sun|moon|thunder|fire|time|wind|sea|river|flavor|wave|willow|rain|tree|flower|field|meadow|pasture|harvest|water|father|mother|brother|sister <VERB>|<PREPOSITION>|$END",
			"PRONOUN: my|your|his|her <NOUN>|<ADJECTIVE>",
			"VERB: runs|walks|stands|climbs|crawls|flows|flies|transcends|ascends|descends|sinks <PREPOSITION>|<PRONOUN>|$END",
			"PREPOSITION: above|across|against|along|among|around|before|behind|beneath|beside|between|beyond|during|inside|onto|outside|under|underneath|upon|with|without|through <NOUN>|<PRONOUN>|<ADJECTIVE>" };

	public static void main(String[] args) throws URISyntaxException, IOException {
		
		if(args.length < 1){
			System.out.println("Use: java -jar PoemGenerator.jar grammar_file.txt");
			return;
		}
		Map<String, PoemTree> map = new HashMap<>();
//		for (String line : lines) {
//			addDefinition(map, line);
//		}
		
		FileInputStream fis = new FileInputStream(getGrammarFile(args));
		 
		//Construct BufferedReader from InputStreamReader
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
	 
		String line = null;
		while ((line = br.readLine()) != null) {
			addDefinition(map, line);
		}
	 
		br.close();
		
		PoemTree poemTree = map.get("POEM");

		System.out.println("  " + poemTree.getPoem());
	}

	public static void addDefinition(Map<String, PoemTree> map, String input) {
		PoemGeneratorLexer lexer = new PoemGeneratorLexer(new ANTLRInputStream(
				input));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		PoemGeneratorParser parser = new PoemGeneratorParser(tokens);

		parser.addParseListener(new PoemGeneratorBaseListener() {

		});
		DefinitionContext definition = parser.definition();

		PoemVisitor poemVisitor = new PoemVisitor(map);
		PoemTree  definitionRule = poemVisitor.visit(definition);

		map.put(definitionRule.getRuleName(), definitionRule.getDefinition());
	}

	public static File getGrammarFile(String[] args) throws URISyntaxException {
		if (args.length == 0)
			return new File(PoemComposer.class.getResource("/grammar/grammar.txt").toURI());
		else
			return new File(args[0]);
	}
}
