package fdariasm.composer;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.tree.ParseTree;

import fdariasm.ast.ComposedWord;
import fdariasm.ast.ConcatDef;
import fdariasm.ast.Definition;
import fdariasm.ast.Keyword;
import fdariasm.ast.Or;
import fdariasm.ast.PoemTree;
import fdariasm.ast.Reference;
import fdariasm.ast.Words;
import fdariasm.parser.PoemGeneratorBaseVisitor;
import fdariasm.parser.PoemGeneratorParser.ComposedwordContext;
import fdariasm.parser.PoemGeneratorParser.ConcatdefContext;
import fdariasm.parser.PoemGeneratorParser.DefinitionContext;
import fdariasm.parser.PoemGeneratorParser.OrContext;
import fdariasm.parser.PoemGeneratorParser.OrdefContext;
import fdariasm.parser.PoemGeneratorParser.ReferenceContext;
import fdariasm.parser.PoemGeneratorParser.WordsContext;

public class PoemVisitor extends PoemGeneratorBaseVisitor<PoemTree> {

	private Map<String, PoemTree> rules;

	public PoemVisitor(Map<String, PoemTree> rules) {
		this.rules = rules;
	}

	@Override
	public PoemTree visitComposedword(ComposedwordContext ctx) {
//		System.out.println("Composed word: " + ctx.getText());

		PoemTree words = ctx.getChild(0).accept(this);
		PoemTree or = ctx.getChild(1).accept(this);

		ComposedWord composed = new ComposedWord(rules, words, or);
		return composed;
	}

	@Override
	public PoemTree visitDefinition(DefinitionContext ctx) {
//		System.out.println("Definition" + ctx.getText());

		PoemTree ruleDef = super.visitDefinition(ctx);
		Definition definition = new Definition(rules,
				ctx.getChild(0).getText(), ruleDef);

		return definition;
	}

	@Override
	public PoemTree visitConcatdef(ConcatdefContext ctx) {
//		System.out.println("ConcatDef: " + ctx.getText());
		List<PoemTree> children = new LinkedList<>();
		for(int i = 0; i < ctx.getChildCount(); i ++){
			ParseTree child = ctx.getChild(i); 
			children.add(child.accept(this));
		}
		ConcatDef concat = new ConcatDef(rules, children);
		
		return concat;
	}

	@Override
	public PoemTree visitReference(ReferenceContext ctx) {
//		System.out.println("Reference: " + ctx.getText());
//		System.out.println("Child: " + ctx.getChild(1));
		Reference reference = new Reference(rules, ctx.getChild(1).getText());
		return reference;
	}

	@Override
	public PoemTree visitOr(OrContext ctx) {
//		System.out.println("Or: " + ctx.getText());
		
		List<PoemTree> children = new LinkedList<>();
		for(int i = 0; i < ctx.getChildCount(); i ++){
			ParseTree child = ctx.getChild(i); 
			if("|".equals(child.getText())) continue;
//			System.out.println("Child: " + child);
			PoemTree orDef = child.accept(this);
//			System.out.println("Res: " + orDef);
			children.add(orDef);
		}
		
		Or or = new Or(rules, children);
		
		return or;
	}

	@Override
	public PoemTree visitOrdef(OrdefContext ctx) {
//		System.out.println("OrDef: " + ctx.getText());
		
		PoemTree ordef = super.visitOrdef(ctx);
//		System.out.println("OrDef: " + ordef);
		if(null == ordef){
			Keyword keyword = new Keyword(rules, ctx.getText());
			return keyword;
		}
		return ordef;
	}

	@Override
	public PoemTree visitWords(WordsContext ctx) {
//		System.out.println("Words: " + ctx.getText());
		List<String> wordsList = new LinkedList<>();
		for(int i = 0; i < ctx.getChildCount(); i ++){
			ParseTree child = ctx.getChild(i);
			String content = child.getText();
			if(!"|".equals(content)){
				wordsList.add(content);
			}
		}
		Words words = new Words(rules, wordsList);
		
		return words;
	}

}
