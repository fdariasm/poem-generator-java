/*
 * @author fdariasm
 */
grammar PoemGenerator;

definition 
	:	IDENT':' (concatdef | composedword)
	;

concatdef
	:	or (or)*
	;

composedword 
	:	words or
	;

or 	:	ordef ('|'ordef)*
	;

ordef 	:	reference | KEYWORD
	;

words	:	WORD ('|'WORD)*
	;
reference
	:	'<' IDENT '>'
	;

WORD 	:	LLETTER (LLETTER)*;

WS : (' ' | '\t' | '\n' | '\r' | '\f')+ -> skip ; // skip spaces, tabs, newlines

KEYWORD :	('$LINEBREAK' | '$END');

IDENT : LETTER (LETTER)*;

fragment LETTER : ('A'..'Z') ;

fragment LLETTER : ('a'..'z') ;