package fdariasm.parser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.antlr.runtime.RecognitionException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.TerminalNode;

import fdariasm.ast.Definition;
import fdariasm.ast.PoemTree;
import fdariasm.composer.PoemVisitor;
import fdariasm.parser.PoemGeneratorParser.ComposedwordContext;
import fdariasm.parser.PoemGeneratorParser.ConcatdefContext;
import fdariasm.parser.PoemGeneratorParser.DefinitionContext;
import fdariasm.parser.PoemGeneratorParser.OrContext;
import fdariasm.parser.PoemGeneratorParser.OrdefContext;
import fdariasm.parser.PoemGeneratorParser.ReferenceContext;
import fdariasm.parser.PoemGeneratorParser.WordsContext;

public class Test {
	
	public static void main_(String[] args) throws RecognitionException, IOException {
		PoemGeneratorLexer lexer = 
				new PoemGeneratorLexer(new ANTLRInputStream(
						"ADJECTIVE: black|white|dark|light|bright|murky|muddy|clear <NOUN>|<ADJECTIVE>|$END"));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		PoemGeneratorParser parser = new PoemGeneratorParser(tokens);
		
		parser.addParseListener(new PoemGeneratorBaseListener(){
			
		});
		DefinitionContext definition = parser.definition();
		
		Map<String, PoemTree> map = new HashMap<>();
		
		PoemVisitor poemVisitor = new PoemVisitor(map);
		Definition definitionRule = (Definition) poemVisitor.visit(definition);
		
		map.put(definitionRule.getRuleName(), definitionRule.getDefinition());
//		MyVisitor visitor = new MyVisitor();
//		visitor.visit(definition);
	}

	public static class MyVisitor extends PoemGeneratorBaseVisitor<Void>{
		
		@Override
		public Void visitComposedword(ComposedwordContext ctx) {
			System.out.println("Composed word: " + ctx.getText());
			System.out.println("Size" + ctx.getChildCount());
			return super.visitComposedword(ctx);
		}
		
		@Override
		public Void visitDefinition(DefinitionContext ctx) {
			System.out.println("Definition: " + ctx.getText());
			System.out.println("Child: " + ctx.getChild(0));
			return super.visitDefinition(ctx);
		}
		
		@Override
		public Void visitConcatdef(ConcatdefContext ctx) {
			System.out.println("ConcatDef: " + ctx.getText());
			for(int i = 0; i < ctx.getChildCount(); i++){
				ParseTree child = ctx.getChild(i);
//				System.out.print(child.getText() + " ");
				child.accept(this);
			}
			System.out.println();
			
			return null;
		}
		
		@Override
		public Void visitReference(ReferenceContext ctx) {
			System.out.println("Reference: " + ctx.getText());
			System.out.println("Child: " + ctx.getChild(1));
			return super.visitReference(ctx);
		}
		
		@Override
		public Void visitTerminal(TerminalNode node) {
//			System.out.println("Terminal: " + node.getText());
			return super.visitTerminal(node);
		}
		
		@Override
		public Void visitOr(OrContext ctx) {
			System.out.println("Or: " + ctx.getText());
			return super.visitOr(ctx);
		}
		
		@Override
		public Void visitOrdef(OrdefContext ctx) {
			System.out.println("OrDef: " + ctx.getText());
			System.out.println("Size: " + ctx.getChildCount());
			
			return super.visitOrdef(ctx);
		}
			
		
		@Override
		public Void visitWords(WordsContext ctx) {
			System.out.println("Words: " + ctx.getText());
			System.out.println("Words: " + ctx.getChild(1));
			return super.visitWords(ctx);
		}
		
		
	}

}
