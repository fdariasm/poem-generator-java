// Generated from C:\Users\fdariasm\PoemGenerator.g4 by ANTLR 4.1
package fdariasm.parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link PoemGeneratorParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface PoemGeneratorVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link PoemGeneratorParser#reference}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReference(@NotNull PoemGeneratorParser.ReferenceContext ctx);

	/**
	 * Visit a parse tree produced by {@link PoemGeneratorParser#concatdef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConcatdef(@NotNull PoemGeneratorParser.ConcatdefContext ctx);

	/**
	 * Visit a parse tree produced by {@link PoemGeneratorParser#or}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOr(@NotNull PoemGeneratorParser.OrContext ctx);

	/**
	 * Visit a parse tree produced by {@link PoemGeneratorParser#ordef}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitOrdef(@NotNull PoemGeneratorParser.OrdefContext ctx);

	/**
	 * Visit a parse tree produced by {@link PoemGeneratorParser#words}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitWords(@NotNull PoemGeneratorParser.WordsContext ctx);

	/**
	 * Visit a parse tree produced by {@link PoemGeneratorParser#definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDefinition(@NotNull PoemGeneratorParser.DefinitionContext ctx);

	/**
	 * Visit a parse tree produced by {@link PoemGeneratorParser#composedword}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitComposedword(@NotNull PoemGeneratorParser.ComposedwordContext ctx);
}