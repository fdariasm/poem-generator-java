// Generated from C:\Users\fdariasm\PoemGenerator.g4 by ANTLR 4.1
package fdariasm.parser;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PoemGeneratorParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__3=1, T__2=2, T__1=3, T__0=4, WORD=5, WS=6, KEYWORD=7, IDENT=8;
	public static final String[] tokenNames = {
		"<INVALID>", "':'", "'<'", "'|'", "'>'", "WORD", "WS", "KEYWORD", "IDENT"
	};
	public static final int
		RULE_definition = 0, RULE_concatdef = 1, RULE_composedword = 2, RULE_or = 3, 
		RULE_ordef = 4, RULE_words = 5, RULE_reference = 6;
	public static final String[] ruleNames = {
		"definition", "concatdef", "composedword", "or", "ordef", "words", "reference"
	};

	@Override
	public String getGrammarFileName() { return "PoemGenerator.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public PoemGeneratorParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class DefinitionContext extends ParserRuleContext {
		public ComposedwordContext composedword() {
			return getRuleContext(ComposedwordContext.class,0);
		}
		public ConcatdefContext concatdef() {
			return getRuleContext(ConcatdefContext.class,0);
		}
		public TerminalNode IDENT() { return getToken(PoemGeneratorParser.IDENT, 0); }
		public DefinitionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_definition; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).enterDefinition(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).exitDefinition(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PoemGeneratorVisitor ) return ((PoemGeneratorVisitor<? extends T>)visitor).visitDefinition(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DefinitionContext definition() throws RecognitionException {
		DefinitionContext _localctx = new DefinitionContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_definition);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(14); match(IDENT);
			setState(15); match(1);
			setState(18);
			switch (_input.LA(1)) {
			case 2:
			case KEYWORD:
				{
				setState(16); concatdef();
				}
				break;
			case WORD:
				{
				setState(17); composedword();
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ConcatdefContext extends ParserRuleContext {
		public OrContext or(int i) {
			return getRuleContext(OrContext.class,i);
		}
		public List<OrContext> or() {
			return getRuleContexts(OrContext.class);
		}
		public ConcatdefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_concatdef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).enterConcatdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).exitConcatdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PoemGeneratorVisitor ) return ((PoemGeneratorVisitor<? extends T>)visitor).visitConcatdef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ConcatdefContext concatdef() throws RecognitionException {
		ConcatdefContext _localctx = new ConcatdefContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_concatdef);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(20); or();
			setState(24);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==2 || _la==KEYWORD) {
				{
				{
				setState(21); or();
				}
				}
				setState(26);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ComposedwordContext extends ParserRuleContext {
		public WordsContext words() {
			return getRuleContext(WordsContext.class,0);
		}
		public OrContext or() {
			return getRuleContext(OrContext.class,0);
		}
		public ComposedwordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_composedword; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).enterComposedword(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).exitComposedword(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PoemGeneratorVisitor ) return ((PoemGeneratorVisitor<? extends T>)visitor).visitComposedword(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ComposedwordContext composedword() throws RecognitionException {
		ComposedwordContext _localctx = new ComposedwordContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_composedword);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(27); words();
			setState(28); or();
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrContext extends ParserRuleContext {
		public OrdefContext ordef(int i) {
			return getRuleContext(OrdefContext.class,i);
		}
		public List<OrdefContext> ordef() {
			return getRuleContexts(OrdefContext.class);
		}
		public OrContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_or; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).enterOr(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).exitOr(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PoemGeneratorVisitor ) return ((PoemGeneratorVisitor<? extends T>)visitor).visitOr(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrContext or() throws RecognitionException {
		OrContext _localctx = new OrContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_or);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(30); ordef();
			setState(35);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==3) {
				{
				{
				setState(31); match(3);
				setState(32); ordef();
				}
				}
				setState(37);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class OrdefContext extends ParserRuleContext {
		public ReferenceContext reference() {
			return getRuleContext(ReferenceContext.class,0);
		}
		public TerminalNode KEYWORD() { return getToken(PoemGeneratorParser.KEYWORD, 0); }
		public OrdefContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_ordef; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).enterOrdef(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).exitOrdef(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PoemGeneratorVisitor ) return ((PoemGeneratorVisitor<? extends T>)visitor).visitOrdef(this);
			else return visitor.visitChildren(this);
		}
	}

	public final OrdefContext ordef() throws RecognitionException {
		OrdefContext _localctx = new OrdefContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_ordef);
		try {
			setState(40);
			switch (_input.LA(1)) {
			case 2:
				enterOuterAlt(_localctx, 1);
				{
				setState(38); reference();
				}
				break;
			case KEYWORD:
				enterOuterAlt(_localctx, 2);
				{
				setState(39); match(KEYWORD);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class WordsContext extends ParserRuleContext {
		public List<TerminalNode> WORD() { return getTokens(PoemGeneratorParser.WORD); }
		public TerminalNode WORD(int i) {
			return getToken(PoemGeneratorParser.WORD, i);
		}
		public WordsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_words; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).enterWords(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).exitWords(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PoemGeneratorVisitor ) return ((PoemGeneratorVisitor<? extends T>)visitor).visitWords(this);
			else return visitor.visitChildren(this);
		}
	}

	public final WordsContext words() throws RecognitionException {
		WordsContext _localctx = new WordsContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_words);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(42); match(WORD);
			setState(47);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==3) {
				{
				{
				setState(43); match(3);
				setState(44); match(WORD);
				}
				}
				setState(49);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ReferenceContext extends ParserRuleContext {
		public TerminalNode IDENT() { return getToken(PoemGeneratorParser.IDENT, 0); }
		public ReferenceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_reference; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).enterReference(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof PoemGeneratorListener ) ((PoemGeneratorListener)listener).exitReference(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof PoemGeneratorVisitor ) return ((PoemGeneratorVisitor<? extends T>)visitor).visitReference(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ReferenceContext reference() throws RecognitionException {
		ReferenceContext _localctx = new ReferenceContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_reference);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(50); match(2);
			setState(51); match(IDENT);
			setState(52); match(4);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\uacf5\uee8c\u4f5d\u8b0d\u4a45\u78bd\u1b2f\u3378\3\n9\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\3\2\3\2\3\2\3\2\5\2\25\n\2"+
		"\3\3\3\3\7\3\31\n\3\f\3\16\3\34\13\3\3\4\3\4\3\4\3\5\3\5\3\5\7\5$\n\5"+
		"\f\5\16\5\'\13\5\3\6\3\6\5\6+\n\6\3\7\3\7\3\7\7\7\60\n\7\f\7\16\7\63\13"+
		"\7\3\b\3\b\3\b\3\b\3\b\2\t\2\4\6\b\n\f\16\2\2\66\2\20\3\2\2\2\4\26\3\2"+
		"\2\2\6\35\3\2\2\2\b \3\2\2\2\n*\3\2\2\2\f,\3\2\2\2\16\64\3\2\2\2\20\21"+
		"\7\n\2\2\21\24\7\3\2\2\22\25\5\4\3\2\23\25\5\6\4\2\24\22\3\2\2\2\24\23"+
		"\3\2\2\2\25\3\3\2\2\2\26\32\5\b\5\2\27\31\5\b\5\2\30\27\3\2\2\2\31\34"+
		"\3\2\2\2\32\30\3\2\2\2\32\33\3\2\2\2\33\5\3\2\2\2\34\32\3\2\2\2\35\36"+
		"\5\f\7\2\36\37\5\b\5\2\37\7\3\2\2\2 %\5\n\6\2!\"\7\5\2\2\"$\5\n\6\2#!"+
		"\3\2\2\2$\'\3\2\2\2%#\3\2\2\2%&\3\2\2\2&\t\3\2\2\2\'%\3\2\2\2(+\5\16\b"+
		"\2)+\7\t\2\2*(\3\2\2\2*)\3\2\2\2+\13\3\2\2\2,\61\7\7\2\2-.\7\5\2\2.\60"+
		"\7\7\2\2/-\3\2\2\2\60\63\3\2\2\2\61/\3\2\2\2\61\62\3\2\2\2\62\r\3\2\2"+
		"\2\63\61\3\2\2\2\64\65\7\4\2\2\65\66\7\n\2\2\66\67\7\6\2\2\67\17\3\2\2"+
		"\2\7\24\32%*\61";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}