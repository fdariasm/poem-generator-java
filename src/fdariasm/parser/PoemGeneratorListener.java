// Generated from C:\Users\fdariasm\PoemGenerator.g4 by ANTLR 4.1
package fdariasm.parser;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link PoemGeneratorParser}.
 */
public interface PoemGeneratorListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link PoemGeneratorParser#reference}.
	 * @param ctx the parse tree
	 */
	void enterReference(@NotNull PoemGeneratorParser.ReferenceContext ctx);
	/**
	 * Exit a parse tree produced by {@link PoemGeneratorParser#reference}.
	 * @param ctx the parse tree
	 */
	void exitReference(@NotNull PoemGeneratorParser.ReferenceContext ctx);

	/**
	 * Enter a parse tree produced by {@link PoemGeneratorParser#concatdef}.
	 * @param ctx the parse tree
	 */
	void enterConcatdef(@NotNull PoemGeneratorParser.ConcatdefContext ctx);
	/**
	 * Exit a parse tree produced by {@link PoemGeneratorParser#concatdef}.
	 * @param ctx the parse tree
	 */
	void exitConcatdef(@NotNull PoemGeneratorParser.ConcatdefContext ctx);

	/**
	 * Enter a parse tree produced by {@link PoemGeneratorParser#or}.
	 * @param ctx the parse tree
	 */
	void enterOr(@NotNull PoemGeneratorParser.OrContext ctx);
	/**
	 * Exit a parse tree produced by {@link PoemGeneratorParser#or}.
	 * @param ctx the parse tree
	 */
	void exitOr(@NotNull PoemGeneratorParser.OrContext ctx);

	/**
	 * Enter a parse tree produced by {@link PoemGeneratorParser#ordef}.
	 * @param ctx the parse tree
	 */
	void enterOrdef(@NotNull PoemGeneratorParser.OrdefContext ctx);
	/**
	 * Exit a parse tree produced by {@link PoemGeneratorParser#ordef}.
	 * @param ctx the parse tree
	 */
	void exitOrdef(@NotNull PoemGeneratorParser.OrdefContext ctx);

	/**
	 * Enter a parse tree produced by {@link PoemGeneratorParser#words}.
	 * @param ctx the parse tree
	 */
	void enterWords(@NotNull PoemGeneratorParser.WordsContext ctx);
	/**
	 * Exit a parse tree produced by {@link PoemGeneratorParser#words}.
	 * @param ctx the parse tree
	 */
	void exitWords(@NotNull PoemGeneratorParser.WordsContext ctx);

	/**
	 * Enter a parse tree produced by {@link PoemGeneratorParser#definition}.
	 * @param ctx the parse tree
	 */
	void enterDefinition(@NotNull PoemGeneratorParser.DefinitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link PoemGeneratorParser#definition}.
	 * @param ctx the parse tree
	 */
	void exitDefinition(@NotNull PoemGeneratorParser.DefinitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link PoemGeneratorParser#composedword}.
	 * @param ctx the parse tree
	 */
	void enterComposedword(@NotNull PoemGeneratorParser.ComposedwordContext ctx);
	/**
	 * Exit a parse tree produced by {@link PoemGeneratorParser#composedword}.
	 * @param ctx the parse tree
	 */
	void exitComposedword(@NotNull PoemGeneratorParser.ComposedwordContext ctx);
}